# AegisBot #

Slack & SMS Reminder

### What is this project for? ###

* This project is for the purpose to remind busy developers of important things to do.
* Version 1.0

### Setup ###

* Clone this repository
```
$ git clone git@bitbucket.org:angelicafrances/wwhocode_hackathone.git
```

* After cloning, go to the designated folder. This is the sample directory I created.
```
$ cd Desktop/lambda
```

* Install Ipython
```
$ pip install ipython
```

* Now that Ipython is installed, run ipython
```
$ ipython
```

* You will then be directed to the console.


### Running the Program on the ipython ###

* Once you are in the console, you can now start writing the program with the input of the user's choice
```
In [1]: import boto3
   ...: import json
   ...: import requests

```

* After the imports, give inputs for the payload, url and headers
```
In [2]:  payload = {'token':'xoxp-277418312325-277274477314-277354541826-79611e80c4d099756e55cc812529e675', 'text':'aegis', 'time':'7:38', 'user
   ...: ':'U8582E198'}
   ...:  url = 'https://slack.com/api/reminders.add'
   ...:  headers = {'Content-Type': 'application/x-www-form-urlencoded'}
   ...:  r = requests.post(url, data=payload, headers=headers)
   ...: 
```

* Input the above into one variable
```
In [3]: r = requests.post(url, data=payload, headers=headers)
```

* Print the output that will be fetched by the Front  End
```
In [4]: print r.content
{"ok":true,"reminder":{"id":"Rm84QK1YG0","creator":"U8582E198","user":"U8582E198","text":"aegis","recurring":false,"time":1511653080,"complete_ts":0}}
```
