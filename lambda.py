# Include the package
# This the "dpw-clinic" lambda function

import boto3
import json
import requests

# print("Loading function")


client = boto3.client('lambda')

def post(text="", time=""):
  # print("--post--")
  payload = {'token':'xoxp-277418312325-277274477314-277354541826-79611e80c4d099756e55cc812529e675', 'text':text, 'time':time, 'user':'U8582E198'}
  url = 'https://slack.com/api/reminders.add'
  headers = {'Content-Type': 'application/x-www-form-urlencoded'}
  r = requests.post(url, data=payload, headers=headers)

  print (payload)
  print r.content
  # print("--post end--")
  return True


def lambda_handler(event, context):
    print("--log start--")
    is_initialized = True
    print("event: {}".format(event))
    print event.get("text", "default")
    print event.get("time", "default")
    text = event.get("text", "default")
    time = str(event.get("time", "default"))
    print("event: {}".format(text))
    print("event: {}".format(time))
    print(type(text))
    print(type(time))
    hello = post(text, time)
    print("--log end--")
    return True
